package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.fragment.NavHostFragment;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;



    MutableLiveData<Book> book;

    public void setSelectedBook(long id) {
        repository.getBook(id);
        book = repository.getSelectedBook();
    }

    public DetailViewModel(@NonNull Application application)
    {
        super(application);
        repository = new BookRepository(application);
        book = repository.getSelectedBook();

    }
    MutableLiveData<Book> getBook(){return book;}

    public void insertOrUpdateBook(Book book)
    {
        if((book != null))
        {
            if((book.getId() != -1))
            {
                repository.updateBook(book);

            }
            else {

                book.setId(repository.countAllBooks() + 1);
                repository.insertBook(book);
            }
        }

    }
}
