package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<List<Book>> searchResults = new MutableLiveData<>();
    private MutableLiveData<Book> selectedBook = new MutableLiveData<>() ;
    private LiveData<List<Book>> allBooks;
    private BookDao bookDao;
    private MutableLiveData<Integer> count = new MutableLiveData<>();

    public BookRepository(Application application)
    {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public void updateBook(Book book)
    {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }

    public void insertBook(Book newbook)
    {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public void getBook(long id)
    {
        Future<Book> fbook = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbook.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public LiveData<List<Book>> getAllBooks()
    {
        return allBooks;
    }

    public void deleteAllBooks() {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteAllBooks();
        });
    }


    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    public Integer countAllBooks()
    {
        Future<Integer> c = databaseWriteExecutor.submit(bookDao::countAllBooks);
        try
        {
            return c.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

}
