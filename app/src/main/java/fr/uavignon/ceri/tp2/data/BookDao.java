package fr.uavignon.ceri.tp2.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BookDao {
    @Update
    void updateBook(Book book);
    @Insert
    void insertBook(Book book);
    @Query("SELECT * from books WHERE id = :id")
    Book getBook(long id);
    @Query("DELETE from books where id = :id")
    void deleteBook(long id);
    @Query("SELECT * from books")
    LiveData<List<Book>> getAllBooks();

    @Query("DELETE from books ")
    void deleteAllBooks();

    @Query("SELECT COUNT(*) from books")
    int countAllBooks();
}
